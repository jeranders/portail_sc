<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 11/12/2017
 */

namespace App\Form\Type;

use App\Entity\ImageManufacturer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class ImageManufacturerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('file', FileType::class, array(
            'label' => false,
            'attr' => array(
                'accept' => 'image/*',
                'capture' => '',
            ),
            'constraints' => new Assert\Image(),
        ))
    ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ImageManufacturer::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'image_manufacturer_type';
    }
}