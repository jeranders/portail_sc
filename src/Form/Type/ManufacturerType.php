<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 09/12/2017
 */

namespace App\Form\Type;


use App\Entity\Manufacturer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ManufacturerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du constructeur *',
                'attr' => [
                    'class' => 'form-control title-seo-url',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Le nom doit avoir un minimum de 3 caractères'
                    ])
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('metatitle', TextType::class, [
                'label' => 'Meta tittle',
                'required' => false,
                'attr' => [
                    'class' => 'form-control title-seo',
                ],
                'constraints' => [
                    new Length([
                        'max' => 60,
                        'maxMessage' => 'Le nom doit avoir un maximum de 60 caractères'
                    ])
                ]
            ])
            ->add('metadescription', TextareaType::class, [
                'label' => 'Meta description',
                'required' => false,
                'attr' => [
                    'class' => 'form-control description-seo',
                ],
                'constraints' => [
                    new Length([
                        'max' => 160,
                        'maxMessage' => 'La description doit avoir un maximum de 160 caractères'
                    ])
                ]
            ])
            ->add('image', ImageManufacturerType::class, [
                'required' => false,
                'label' => 'Logo'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Manufacturer::class,
        ]);
    }
}