<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 20/12/2017
 */

namespace App\Form\Type;

use App\Entity\Ship;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ShipType
 * @package App\Form\Type
 */
class ShipType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du vaisseau *',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('manufacturer', EntityType::class, [
                'label' => 'Fabricant *',
                'class' => 'App\Entity\Manufacturer',
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('height', TextType::class, [
                'label' => 'Hauteur',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('length', TextType::class, [
                'label' => 'Longueur',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('width', TextType::class, [
                'label' => 'Largeur',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('mass', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('mincrew', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('maxcrew', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('cargo', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('scmspeed', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('afterburnerspeed', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('pitchmax', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('yawmax', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('rollmax', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('xaxis', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('yaxis', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('zaxis', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ship::class
        ]);
    }
}