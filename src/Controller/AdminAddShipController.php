<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 20/12/2017
 */

namespace App\Controller;


use App\Service\ShipService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminAddShipController extends AbstractController
{
    /**
     * @Route("/admin/ajout_vaisseau", name="addShip")
     * @param Request $request
     * @param ShipService $shipService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, ShipService $shipService)
    {
        $form = $shipService->addShip($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->addFlash('notice', 'Vaisseau ajouté !');
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/addShip.html.twig', [
            'form' => $form->createView()
        ]);
    }
}