<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 10/12/2017
 */

namespace App\Controller;


use App\Service\ManufacturerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminListManufacturer
 * @package App\Controller
 */
class AdminListManufacturer extends AbstractController
{
    /**
     * @Route("/admin/liste_fabricants", name="listManufacturer")
     * @param ManufacturerService $manufacturerService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ManufacturerService $manufacturerService)
    {
        $list = $manufacturerService->listManufacturer();

        return $this->render('admin/listManufacturer.html.twig', [
            'manufacturers' => $list
        ]);
    }
}