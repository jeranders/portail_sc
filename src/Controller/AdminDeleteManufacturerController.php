<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 09/12/2017
 */

namespace App\Controller;


use App\Service\ManufacturerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminAddManufacturerController
 * @package App\Controller
 */
class AdminDeleteManufacturerController extends AbstractController
{
    /**
     * @Route("/admin/fabricant/delete/{id}", name="deleteManufacturer")
     * @param ManufacturerService $manufacturerService
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function index(ManufacturerService $manufacturerService, $id)
    {
        $manufacturerService->deleteManufacturer($id);

        return $this->redirectToRoute('listManufacturer');
    }
}