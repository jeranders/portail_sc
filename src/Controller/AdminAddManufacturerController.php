<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 09/12/2017
 */

namespace App\Controller;


use App\Service\ManufacturerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminAddManufacturerController
 * @package App\Controller
 */
class AdminAddManufacturerController extends AbstractController
{
    /**
     * @Route("/admin/ajout_fabricant", name="addManufacturer")
     * @param Request $request
     * @param ManufacturerService $manufacturerService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, ManufacturerService $manufacturerService)
    {
        $form = $manufacturerService->addManufacturer($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->addFlash('success', 'Ajout du fabricant ok');
            return $this->redirectToRoute('listManufacturer');
        }

        return $this->render('admin/addManufacturer.html.twig', [
            'form' => $form->createView()
        ]);
    }
}