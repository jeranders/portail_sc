<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 10/12/2017
 */

namespace App\Controller;


use App\Service\ShipService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminListShip
 * @package App\Controller
 */
class AdminListShip extends AbstractController
{
    /**
     * @Route("/admin/liste_vaisseaux", name="listShip")
     * @param ShipService $shipService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ShipService $shipService)
    {
        $list = $shipService->listShip();

        return $this->render('admin/listShip.html.twig', [
            'ships' => $list
        ]);
    }
}