<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 09/12/2017
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminDefaultController
 * @package App\Controller
 */
class AdminDefaultController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig');
    }
}