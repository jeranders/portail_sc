<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 10/12/2017
 */

namespace App\Controller;


use App\Service\ManufacturerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminEditManufacturer
 * @package App\Controller
 */
class AdminEditManufacturer extends AbstractController
{
    /**
     * @Route("admin/fabricant/edit/{slug}", name="editManufacturer")
     * @param ManufacturerService $manufacturerService
     * @param Request $request
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ManufacturerService $manufacturerService, Request $request, $slug)
    {
        $form = $manufacturerService->editManufacturer($request, $slug);
        $manufacturer = $manufacturerService->showNameImageManufacturer($slug);

        if ($form->isSubmitted() && $form->isValid()){
            $this->addFlash('success', 'Modification du fabricant ok');
            return $this->redirectToRoute('showManufacturer', ['slug' => $slug]);
        }

        return $this->render('admin/editManufacturer.html.twig', [
            'form' => $form->createView(),
            'manufacturer' => $manufacturer
        ]);
    }
}