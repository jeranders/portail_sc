<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 10/12/2017
 */

namespace App\Controller;


use App\Service\ManufacturerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminEditManufacturer
 * @package App\Controller
 */
class AdminShowManufacturer extends AbstractController
{
    /**
     * @Route("admin/fabricant/{slug}", name="showManufacturer")
     * @param ManufacturerService $manufacturerService
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ManufacturerService $manufacturerService, $slug)
    {
        $manufacturer = $manufacturerService->showManufacturer($slug);

        return $this->render('admin/showManufacturer.html.twig', [
            'manufacturer' => $manufacturer
        ]);
    }
}