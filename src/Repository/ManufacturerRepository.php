<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 09/12/2017
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class ManufacturerRepository
 * @package App\Repository
 */
class ManufacturerRepository extends EntityRepository
{
    /**
     * Affiche le nom d'un fabricant
     * @param $slug
     * @return mixed
     */
    public function showNameManufacturer($slug)
    {
        try {
            return $this->createQueryBuilder('m')
                ->select('m.name')
                ->addSelect('m.slug')
                ->where('m.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        };
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function showNameImageManufacturer($slug)
    {
        try {
            return $this->createQueryBuilder('m')
                ->select('m.name')
                ->addSelect('m.slug')
                ->addSelect('m.image')
                ->where('m.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        };
    }

    /**
     * Affiche un fabricant par son slug
     * @param $slug
     * @return mixed
     */
    public function showManufacturerBySlug($slug)
    {
        try {
            return $this->createQueryBuilder('m')
                ->where('m.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }
}