<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 20/12/2017
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Ship
 * @package App\Entity
 * @ORM\Table(name="ship")
 * @ORM\Entity(repositoryClass="App\Repository\ShipRepository")
 * @UniqueEntity(fields="name", message="Le nom du vaisseau est déjà utilisée !")
 */
class Ship
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Manufacturer")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id",onDelete="cascade")
     */
    private $manufacturer;

    private $size;

    /**
     * @var int
     * @ORM\Column(name="length", type="float", nullable=true)
     */
    private $length;

    /**
     * @var int
     * @ORM\Column(name="width", type="float", nullable=true)
     */
    private $width;

    /**
     * @var int
     * @ORM\Column(name="height", type="float", nullable=true)
     */
    private $height;

    /**
     * @var int
     * @ORM\Column(name="mass", type="float", nullable=true)
     */
    private $mass;

    /**
     * @var int
     * @ORM\Column(name="cargo", type="integer", nullable=true)
     */
    private $cargo;

    /**
     * @var int
     * @ORM\Column(name="mincrew", type="integer", nullable=true)
     */
    private $mincrew;

    /**
     * @var int
     * @ORM\Column(name="maxcrew", type="integer", nullable=true)
     */
    private $maxcrew;

    /**
     * @var int
     * @ORM\Column(name="scmspeed", type="float", nullable=true)
     */
    private $scmspeed;

    /**
     * @var int
     * @ORM\Column(name="afterburnerspeed", type="float", nullable=true)
     */
    private $afterburnerspeed;

    /**
     * @var int
     * @ORM\Column(name="pitchmax", type="float", nullable=true)
     */
    private $pitchmax;

    /**
     * @var int
     * @ORM\Column(name="yawmax", type="float", nullable=true)
     */
    private $yawmax;

    /**
     * @var int
     * @ORM\Column(name="rollmax", type="float", nullable=true)
     */
    private $rollmax;

    /**
     * @var int
     * @ORM\Column(name="xaxis", type="float", nullable=true)
     */
    private $xaxis;

    /**
     * @var int
     * @ORM\Column(name="yaxis", type="float", nullable=true)
     */
    private $yaxis;

    /**
     * @var int
     * @ORM\Column(name="zaxis", type="float", nullable=true)
     */
    private $zaxis;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param int $manufacturer
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param int $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getMass()
    {
        return $this->mass;
    }

    /**
     * @param int $mass
     */
    public function setMass($mass)
    {
        $this->mass = $mass;
    }

    /**
     * @return int
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * @param int $cargo
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;
    }

    /**
     * @return int
     */
    public function getMincrew()
    {
        return $this->mincrew;
    }

    /**
     * @param int $mincrew
     */
    public function setMincrew($mincrew)
    {
        $this->mincrew = $mincrew;
    }

    /**
     * @return int
     */
    public function getMaxcrew()
    {
        return $this->maxcrew;
    }

    /**
     * @param int $maxcrew
     */
    public function setMaxcrew($maxcrew)
    {
        $this->maxcrew = $maxcrew;
    }

    /**
     * @return int
     */
    public function getScmspeed()
    {
        return $this->scmspeed;
    }

    /**
     * @param int $scmspeed
     */
    public function setScmspeed($scmspeed)
    {
        $this->scmspeed = $scmspeed;
    }

    /**
     * @return int
     */
    public function getAfterburnerspeed()
    {
        return $this->afterburnerspeed;
    }

    /**
     * @param int $afterburnerspeed
     */
    public function setAfterburnerspeed($afterburnerspeed)
    {
        $this->afterburnerspeed = $afterburnerspeed;
    }

    /**
     * @return int
     */
    public function getPitchmax()
    {
        return $this->pitchmax;
    }

    /**
     * @param int $pitchmax
     */
    public function setPitchmax($pitchmax)
    {
        $this->pitchmax = $pitchmax;
    }

    /**
     * @return int
     */
    public function getYawmax()
    {
        return $this->yawmax;
    }

    /**
     * @param int $yawmax
     */
    public function setYawmax($yawmax)
    {
        $this->yawmax = $yawmax;
    }

    /**
     * @return int
     */
    public function getRollmax()
    {
        return $this->rollmax;
    }

    /**
     * @param int $rollmax
     */
    public function setRollmax($rollmax)
    {
        $this->rollmax = $rollmax;
    }

    /**
     * @return int
     */
    public function getXaxis()
    {
        return $this->xaxis;
    }

    /**
     * @param int $xaxis
     */
    public function setXaxis($xaxis)
    {
        $this->xaxis = $xaxis;
    }

    /**
     * @return int
     */
    public function getYaxis()
    {
        return $this->yaxis;
    }

    /**
     * @param int $yaxis
     */
    public function setYaxis($yaxis)
    {
        $this->yaxis = $yaxis;
    }

    /**
     * @return int
     */
    public function getZaxis()
    {
        return $this->zaxis;
    }

    /**
     * @param int $zaxis
     */
    public function setZaxis($zaxis)
    {
        $this->zaxis = $zaxis;
    }




}