<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 09/12/2017
 */

namespace App\Service;

use App\Entity\Manufacturer;
use App\Form\Type\ManufacturerType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ManufacturerService
 * @package App\Service
 */
class ManufacturerService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var FormFactoryInterface
     */
    private $form;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var FileUploader
     */
    private $fileuploader;

    /**
     * @var SlugService
     */
    private $slugService;

    /**
     * ShipService constructor.
     * @param EntityManagerInterface $em
     * @param FormFactoryInterface $form
     * @param SessionInterface $session
     * @param FileUploader $fileuploader
     * @param SlugService $slugService
     */
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $form, SessionInterface $session, FileUploader $fileuploader, SlugService $slugService)
    {
        $this->em = $em;
        $this->form = $form;
        $this->session = $session;
        $this->fileuploader = $fileuploader;
        $this->slugService = $slugService;
    }

    /**
     * Ajout d'un fabricant
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addManufacturer(Request $request)
    {
        $manufacturer = new Manufacturer();

        $form = $this->form->create(ManufacturerType::class, $manufacturer);
        $form->handleRequest($request);

        $form->getData();
        if ($form->isSubmitted() && $form->isValid()){

            if($form['image']->getData() != null){
                $file = $form['image']->getData()->getFile();
                $fileName = $this->fileuploader->upload($file);
                $manufacturer->getImage()->setAlt($fileName);
            }

            $slug = $this->slugService->Slug($manufacturer->getName());
            $manufacturer->setSlug($slug);

            $this->em->persist($manufacturer);
            $this->em->flush();
        }

        return $form;
    }

    /**
     * @param Request $request
     * @param $slug
     * @return \Symfony\Component\Form\FormInterface
     */
    public function editManufacturer(Request $request, $slug)
    {
        $manufacturer =  $this->em
            ->getRepository(Manufacturer::class)
            ->showManufacturerBySlug($slug);

        $form = $this->form->create(ManufacturerType::class, $manufacturer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $form->getData();

            if($form['image']->getData()->getFile() != null){
                $file = $form['image']->getData()->getFile();
                $fileName = $this->fileuploader->upload($file);
                $manufacturer->getImage()->setAlt($fileName);
            }

            $this->em->flush();
        }

        return $form;
    }


    /**
     * Liste des fabricants
     * @return Manufacturer[]|array
     */
    public function listManufacturer()
    {
        $list = $this->em->getRepository(Manufacturer::class)->findAll();

        return $list;
    }

    /**
     * Suppression d'un fabricant
     * @param $id
     * @return string
     */
    public function deleteManufacturer($id)
    {
        $delete = $this->em->getRepository(Manufacturer::class)->find($id);

        if ($delete != null){
            $this->em->remove($delete);
            $this->em->flush();
        }else{
            return $this->session->getFlashBag()->add('success', 'Attention il est impossible de supprimer ce fabricant.');
        }

    }

    /**
     * Affiche la page d'un fabricant
     * @param $slug
     * @return Manufacturer|null|object
     */
    public function showManufacturer($slug)
    {
        $show = $this->em->getRepository(Manufacturer::class)->showManufacturerBySlug($slug);

        return $show;
    }

    /**
     * Affiche le nom d'un fabricant
     * @param $id
     * @return Manufacturer|null|object
     */
    public function showNameManufacturer($id)
    {
        $show = $this->em->getRepository(Manufacturer::class)->showNameManufacturer($id);

        return $show;
    }

    /**
     * Affiche le nom d'un fabricant + l'image
     * @param $id
     * @return mixed
     */
    public function showNameImageManufacturer($id)
    {
        $show = $this->em->getRepository(Manufacturer::class)->showNameImageManufacturer($id);

        return $show;
    }


}