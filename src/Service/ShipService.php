<?php
/**
 * Created by Brechoire Jérôme
 * Email: brechoire.j@gmail.com
 * Date: 09/12/2017
 */

namespace App\Service;


use App\Entity\Manufacturer;
use App\Entity\Ship;
use App\Form\Type\ShipType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ShipService
 * @package App\Service
 */
class ShipService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var FormFactoryInterface
     */
    private $form;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var FileUploader
     */
    private $fileuploader;

    /**
     * @var SlugService
     */
    private $slugService;

    /**
     * ShipService constructor.
     * @param EntityManagerInterface $em
     * @param FormFactoryInterface $form
     * @param SessionInterface $session
     * @param FileUploader $fileuploader
     * @param SlugService $slugService
     */
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $form, SessionInterface $session, FileUploader $fileuploader, SlugService $slugService)
    {
        $this->em = $em;
        $this->form = $form;
        $this->session = $session;
        $this->fileuploader = $fileuploader;
        $this->slugService = $slugService;
    }

    /**
     * Ajout d'un vaisseau
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addShip(Request $request)
    {
        $ship = new Ship();

        $form = $this->form->create(ShipType::class, $ship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($ship);
            $this->em->flush();
        }

        return $form;
    }

    /**
     * Liste des vaisseaux
     * @return Manufacturer[]|Ship[]|array
     */
    public function listShip()
    {
        $list = $this->em->getRepository(Ship::class)->findAll();

        return $list;
    }

}