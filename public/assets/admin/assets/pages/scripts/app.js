$(document).ready(function(e) {

    /**
     * Vérification du formulaire d'ajout et édition d'un fabricant
     */
    $('#manufacturer_name').keyup(function() {

        var nombreCaractere = $(this).val().length;

        if (nombreCaractere > 4) {
            $('#name-manufacturer').addClass("has-success");
            $('#name-manufacturer').removeClass("has-error");
        }else{
            $('#name-manufacturer').removeClass("has-success");
            $('#name-manufacturer').removeClass("has-error");
        }

    })

});