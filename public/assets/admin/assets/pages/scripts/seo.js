// Module SEO by Jérôme Brechoire

$(document).ready(function () {

    function Slug(url)
    {    var result = url.replace(" ", "-");
        return result;
    }

    $(function () {
        var theText = $(".title-seo-url");
        var theOutputText = $(".preview-url-slug");
        var theOutputKeyPress = $("#theOutputKeyPress");
        var theOutputKeyDown = $("#theOutputKeyDown");
        var theOutputKeyUp = $("#theOutputKeyUp");

        theText.keydown(function (event) {
            keyReport(event, theOutputKeyDown);
        });

        theText.keypress(function (event) {
            keyReport(event, theOutputKeyPress);
        });

        theText.keyup(function (event) {
            keyReport(event, theOutputKeyUp);
        });

        function keyReport(event, output) {
            // catch enter key = submit (Safari on iPhone=10)
            if (event.which == 10 || event.which == 13) {
                event.preventDefault();
            }
            // show field content
            var urlpreview = Slug(theText);
            theOutputText.text(urlpreview.val());
        }
    });

    $(function(){

        var titleform = $('.title-seo-url').val();

        $('.title-seo-url').keyup(function (e) {
            console.log(titleform);
            if (this.value != titleform) {
                var urlpreview = Slug(titleform).toLowerCase();
                titleform = this.value.replace(/\n/g, "<br />");

                $('.title-seo-url').focusout(function () {
                    $('.preview-url-slug').html(urlpreview );
                });
            }
        });


        var title = $('.title-seo').val();
        $('.title-seo').keyup(function () {
            if (this.value != title) {
                title = this.value.replace(/\n/g, "<br />");
                $('.preview-title-seo').html(title );
            }
        });

        var content = $('.description-seo').val();
        $('.description-seo').keyup(function () {
            if (this.value != title) {
                content = this.value.replace(/\n/g, "<br />");
                $('.preview-description-seo').html(content );
            }
        });
    })

});